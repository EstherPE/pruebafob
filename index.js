//
// NO MODIFICAR LOS NOMBRES NI LOS PARAMETROS
// DE LAS FUNCIONES O LOS TESTS DEJARAN DE FUNCIONAR
//

// HELPER FUNCTIONS

function factores(n) {
    let result = [];
    for (let i = 1; i <= n; i++) {
        if (n % i === 0) {
            result.push(i);
        }
    }
    return result;
}

function primos(n) {
    if (n<=1)
        return [];

    var numArr = primos(n - 1);

    if (primo(n))
        numArr.push(n);

    return numArr;
}

function primo(n) {
    let result = true;

    if (n ===1) {
        return false;
    }
    else if (n > 1) {
        for (let i = 2; i < n; i++) {
            if (n % i === 0) {
                result = false;
                break;
            }
        }
    }
    else {
        console.log('Número introducido incorrecto');
    }

    return result;
}

// KATAS

// Utilice los métodos map(), filter() y reduce() si fuera posible

// Utilizando la función factores(N) que devuelve en un arreglo los factores de un número,
// escriba una función que devuelva otro arreglo con la suma de todos los factores
// de todos los números entre 1 y 15 (ambos incluidos)

function sumaFactores(n) {
    
    let result= [];
    for(let i =1;i<=n;i++){
        let fac = factores(i);
        let sum= fac.reduce((acc,factor) =>{
            return acc+ factor;
        } ,0);
        result.push(sum);
    }
    return result;
}


 //console.log(sumaFactores(15))

// Utilizando la función factores(N) que devuelve en un arreglo los factores de un número,
// escriba una función que devuelva otro arreglo con la cantidad de factores pares
// de todos los números entre 1 y 15 (ambos incluidos)
// Utilice los métodos map() y filter() si fuera posible

function cantidadFactoresPares(n) {

    let result= [];
    for(let i =1;i<=n;i++){
        let fac = factores(i).filter((item)=>item %2 ==0 );
        let par= fac.length
        result.push(par);
    }
    return result;
}

 //console.log(cantidadFactoresPares(15))


// Utilizando la función que determina si un número es primo y la función que extrae
// los factores de un número, calcule la suma de todos los factores primos
// del número 600851475

function sumaFactoresPrimos(n) {
    let sum = factores(n).filter((item)=>primo(item)).reduce((acc,t)=>{
        return acc+t
    },0);
    
    
    return sum;
}
//console.log(sumaFactoresPrimos(600851475))

// Dado un arreglo de enteros, escriba una función mayorEntero() que encuentre el mayor entero en el arreglo
// Por ejemplo, en [3,12,11,56,2,78] debería devolver 78

function mayorEntero(arr) {
    let mayor= arr.reduce((may,item)=>{
        return may > item? may : item;
    },0);
    return mayor
}
// console.log(mayorEntero([3,12,11,56,2,78]))

// Dado un arreglo de enteros, escriba una función menorEntero() que encuentre
// el menor entero en el arreglo
// Por ejemplo, en [3,12,11,56,2,78] debería devolver 2

function menorEntero(arr) {
    let menor= arr.reduce((men,item)=>{
        return men < item? men : item;
    }, mayorEntero(arr));
    return menor
}
// console.log(menorEntero([3,12,11,56,2,78]))

// Dado un arreglo de enteros, escriba una función sinDuplicados() que devuelva
// el arreglo pero sin números duplicados
// Por ejemplo, si el arreglo es [2,33,44,6,17,17,89,44] debería devolver [2,33,44,6,17,89]

function sinDuplicados(arr) {
    let nuevo= [];
    for( item of arr){
        if(nuevo.includes(item)==false){
            nuevo.push(item);
        }
    }
    return nuevo;
}
// console.log(sinDuplicados([2,33,44,6,17,17,89,44]));

// Dado un arreglo de enteros, escriba una función duplicados() que devuelva
// un arreglo de los números que están duplicados en el arreglo
// Por ejemplo, si el arreglo es [2,33,44,6,17,17,89,44] debería devolver [17,44]

function duplicados(arr) {
    let nuevo= [];
    let repetido=[];
    for( item of arr){
        if(nuevo.includes(item)==false){
            nuevo.push(item);
        }else{
            repetido.push(item);
        }
    }
    return repetido;
}
// console.log(duplicados([2,33,44,6,17,17,89,44]));

// La suma de cuadrados de los 10 primeros números naturales es:
//    1**2 + 2**2 + 3**2 ... + 10**2 = 385
// El cuadrado de la suma de los 10 primeros números naturales es:
//    (1 + 2 + 3 + ... + 10)**2 = 55**2 = 3025
// La diferencia entre la suma de cuadrados y el cuadrado de la suma es:
//     3025 - 385 = 2640
// Escriba la función diferenciaCuadrados(N) que calcule esta cantidad
// para los 20 primeros números naturales

function diferenciaCuadrados(n) {
    let sumaCuadrados=0;
    let cuadradoSumas=0;
    for(i=1;i<=n;i++){
        sumaCuadrados+= i**2;
        cuadradoSumas+=i;
    }
    cuadradoSumas=cuadradoSumas**2;
    return cuadradoSumas-sumaCuadrados;
}

//  console.log(diferenciaCuadrados(20))

// Utilizando la función diferenciaCuadrados(N) del ejercicio anterior,
// calcule las diferencias de cuadrados de los 10 primeros números naturales
// y guárdelas en un arreglo

function diferenciasCuadrados(n) {
    let result= [];
    for(j=1;j<=n;j++){
        result.push(diferenciaCuadrados(j));
    }
    return result;
}
//  console.log(diferenciasCuadrados(10))



// Utilizando la función diferenciasCuadrados(N) del ejercicio anterior,
// calcule la suma de las diferencias de cuadrados de los 10 primeros números naturales

function sumaDiferenciasCuadrados(n) {
    return diferenciasCuadrados(n).reduce((acc,item)=> acc+item ,0)
}

// console.log(sumaDiferenciasCuadrados(10))

// La suma de cubos de los 10 primeros números naturales es:
//    1**3 + 2**3 + 3**3 ... + 10**3 = 3025
// El cubo de la suma de los 10 primeros números naturales es:
//    (1 + 2 + 3 + ... + 10)**3 = 55**3 = 166375
// La diferencia entre la suma de cubos y el cubo de la suma es:
//     166375 - 3025 = 163350
// Escriba la función diferenciaCubos(n) que calcule esta cantidad
// para los 20 primeros números naturales

function diferenciaCubos(n) {
    let sumaCubos=0;
    let cuboSumas=0;
    for(i=1;i<=n;i++){
        sumaCubos+= i**3;
        cuboSumas+=i;
    }
    cuboSumas=cuboSumas**3;
    return cuboSumas-sumaCubos;
}

// console.log(diferenciaCubos(20))

// Utilizando la función diferenciaCubos(N) del ejercicio anterior,
// calcule las diferencias de cubos de los 10 primeros números naturales
// y guárdelas en un arreglo

function diferenciasCubos(n) {
    let result= [];
    for(j=1;j<=n;j++){
        result.push(diferenciaCubos(j));
    }
    return result;
}

//  console.log(diferenciasCubos(10))



// Utilizando la función diferenciasCubos(N) del ejercicio anterior,
// calcule la suma de las diferencias de cubos de los 10 primeros números naturales

 function sumaDiferenciasCubos(n) {

    return diferenciasCubos(n).reduce((acc,item)=> acc+item ,0)
}

// console.log(sumaDiferenciasCubos(10))

// En la lista de los números primos: 2, 3, 5, 7, 11, 13, 17, 19, 23, 31 podemos ver que 13 es el sexto
// Escriba una función lugarPrimo(p) que dado un lugar p diga que número primo ocupa esta
// posición
// Por ejemplo: lugarPrimo(6) debería devolver 13 y lugarPrimo(3) debería devolver 5

function lugarPrimo(p) {
    let arrayPrimos= [2, 3, 5, 7, 11, 13, 17, 19, 23, 31];
    return arrayPrimos[p-1]
}
// console.log(lugarPrimo(6))

// Un número triangular es la suma de todos los números entre 1 y el número (ambos incluidos)
// Por ejemplo: el valor triangular de 7 sería 1+2+3+4+5+6+7 = 28
// Escriba una función numeroTriangular(N) que devuelva el valor triangular de 100

function numeroTriangular(n) {
    let sum=0;
    for(i=1;i<=n;i++){
        sum+=i;
    }
    return sum
}

// console.log(numeroTriangular(100))

// Utilizando la función numeroTriangular(N), escriba la función numerosTriangulares(N)
// que devuelva un arreglo con los valores triangulares para todos los números entre 1 y 10

function numerosTriangulares(n) {
    let result=[];
    for(j=1;j<=n;j++){
        result.push(numeroTriangular(j))
    }
    return result;
}

// console.log(numerosTriangulares(10))

// Utilizando la función numerosTriangulares(n), seleccione
// todos los valores triangulares pares y guárdelos en un arreglo
// Utilice si es posible el método filter()
// Haga la prueba con el valor 10

function numerosTriangularesPares(n) {
    return numerosTriangulares(n).filter((item)=> item %2 ==0)
}

// console.log(numerosTriangularesPares(10))

// Utilizando la función numerosTriangulares(n), seleccione
// todos los valores triangulares impares y guárdelos en un arreglo
// Utilice si es posible el método filter()
// Haga la prueba con el valor 10

function numerosTriangularesImpares(n) {
    return numerosTriangulares(n).filter((item)=> item %2 !=0)
}
// console.log(numerosTriangularesImpares(10))

// Utilizando la función numerosTriangulares(n), seleccione
// todos los valores triangulares primos y guárdelos en un arreglo
// Utilice si es posible el método filter()
// Haga la prueba con el valor 100

function numerosTriangularesPrimos(n) {
    return numerosTriangulares(n).filter((item)=> primo(item))
}

// console.log(numerosTriangularesPrimos(100))

// Utilizando la función numerosTriangulares(n), seleccione
// todos los valores triangulares no primos y guárdelos en un arreglo
// Utilice si es posible el método filter()

function numerosTriangularesNoPrimos(n) {
    return numerosTriangulares(n).filter((item)=> !primo(item))
}

// console.log(numerosTriangularesNoPrimos(10))

// Utilizando la función numerosTriangularesNoPrimos(N),
// calcule los factores de cada uno de los valores triangulares y guárdelos en un arreglo
// Llame a la función factoresTriangularesNP(N)
// Utilice si es posible el método map()

function factoresTriangularesNP(n) {

    return numerosTriangularesNoPrimos(n).map((item)=>factores(item))
}

//  console.log(factoresTriangularesNP(4))

// Utilizando la función factoresTriangularesNP(N), calcule el cuadrado
// de la suma de los factores triangulares y guárdelos en un arreglo
// Llame a la función totalTriangularesNP(N)
// Utilice si es posible los métodos map() y reduce()

function totalTriangularesNP(n) {
    return factoresTriangularesNP(n).map((item)=> (item.reduce((acc,j)=>acc+j))**2)
}

// console.log(totalTriangularesNP(4))

// Utilizando la función totalTriangularesNP(N), calcule la suma de todos
// los valores del arreglo
// Utilice si es posible el método reduce()

function sumaTriangularesNP(n) {
    return totalTriangularesNP(n).reduce((acc,t)=>acc+t)
}
// console.log(sumaTriangularesNP(4))

// Escriba una función extraeDigitos(s), donde s es una cadena de caracteres compuesta
// por dígitos y que devuelva un arreglo de números enteros a partir de ellos
// Por ejemplo extraeDigitos(“12345”) debe devolver [1,2,3,4,5]
// El método split() de una cadena pasándole como argumento una cadena vacía,
// devuelve los caracteres de la cadena en forma de arreglo. “abc”.split(“”)
// devuelve [“a”,”b”,”c”]
// Si es posible utilice el método map() y la función parseInt() para convertir
// cada dígito en un número entero

function extraeDigitos(s) {
    return s.split('').map((item)=>parseInt(item))
}

// console.log(extraeDigitos('12345'))

// Usando la función extraeDigitos(s) del ejercicio anterior,
// cree la función sumaDigitos(s) que devuelva la suma de los dígitos
// de un número representado como cadena de caracteres
// Por ejemplo sumaDigitos(“32768”) es 3+2+7+6+8 = 26
// Utilice si es posible el método reduce()

function sumaDigitos(s) {
    return extraeDigitos(s).reduce((acc,i)=>acc+i)
}

// console.log(sumaDigitos('32768'));

// Cree una función contadorLetras(s) que dado un cadena de caracteres s
// devuelva un arreglo de objetos con dos propiedades: caracter y cantidad
// Por ejemplo: contadorLetras(“aaabbcdf”) deberá devolver:
//  [{caracter:”a”, cantidad:3},
//   {caracter:”b”, cantidad:2},
//   {caracter:”c”,cantidad:1},
//   { caracter:”d”,cantidad:1} ,
//   { caracter:”f”,cantidad:1}]
// Utilice el método split(“”) para convertir una cadena en un arreglo de caracteres

function contadorLetras(s) {
    let array= s.split('');
    let aux=[]
    let result=[]
    for(item of array){
        if(!aux.includes(item)){
            aux.push(item);
            result.push({caracter: item, cantidad: 1})
        }else{
            let index= aux.indexOf(item);
            result[index].cantidad+=1;
        }
    }
    return result;
}

// console.log(contadorLetras('aaabbcdf'))

// Cree una función contadorDigitos(s) que dado un cadena de caracteres s devuelva
// un arreglo de objetos con dos propiedades: digito y cantidad
// Por ejemplo: contadorDigitos(“12224433”) deberá devolver
// [{digito:”1”, cantidad:1},
//  {digito:”2”, cantidad:3},
//  {digito:”4”,cantidad:2},
//  {digito:”3”,cantidad:2}]
// Utilice el método split(“”) para convertir una cadena en un arreglo de caracteres

function contadorDigitos(s) {
    let array= s.split('');
    let aux=[]
    let result=[]
    for(item of array){
        if(!aux.includes(item)){
            aux.push(item);
            result.push({caracter: item, cantidad: 1})
        }else{
            let index= aux.indexOf(item);
            result[index].cantidad+=1;
        }
    }
    return result;
}

// console.log(contadorDigitos('12224433'))

export {
    sumaFactores,
    cantidadFactoresPares,
    sumaFactoresPrimos,
    mayorEntero,
    menorEntero,
    sinDuplicados,
    duplicados,
    diferenciaCuadrados,
    diferenciasCuadrados,
    sumaDiferenciasCuadrados,
    diferenciaCubos,
    diferenciasCubos,
    sumaDiferenciasCubos,
    lugarPrimo,
    numeroTriangular,
    numerosTriangulares,
    numerosTriangularesPares,
    numerosTriangularesImpares,
    numerosTriangularesPrimos,
    numerosTriangularesNoPrimos,
    factoresTriangularesNP,
    totalTriangularesNP,
    sumaTriangularesNP,
    extraeDigitos,
    sumaDigitos,
    contadorLetras,
    contadorDigitos
};
